#! /usr/bin/env python3

''' ex11.3b - RESTful API - GET - report '''

import sys
import os
import requests
from pprint import pprint
from urllib3.exceptions import InsecureRequestWarning

rest_url = 'https://netbox.lasthop.io/api/dcim/devices/'
http_headers = {'accept': 'application/json; version=2.4;',
                'Authorization': f"Token {os.environ['NETBOX_TOKEN']}"}

# // Disable SSL certificate warnings (unsigned certificate on server) //
requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

# Get top level endpoints
response = requests.get(rest_url, headers=http_headers, verify=False)

# // Print out requirements from response data //
device_data = response.json()

# // Print report //
for device in device_data['results']:
	print('-' * 60)
	label = device['display_name']
	print(label)
	print('-' * len(label))
	print(f"Location: {device['site']['name']}")
	print(f"Vendor: {device['device_type']['manufacturer']['name']}")
	print(f"Status: {device['status']['label']}")
	print('-' * 60)
	print('\n')

# End
sys.exit()

