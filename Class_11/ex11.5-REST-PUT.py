#! /usr/bin/env python3

''' ex11.5 - RESTful API - POST '''

import sys
import os
import requests
import json
from pprint import pprint
from urllib3.exceptions import InsecureRequestWarning

rest_url = 'https://netbox.lasthop.io/api/ipam/ip-addresses/'
http_headers = {'Content-Type': 'application/json; version=2.4;',
				'accept': 'application/json; version=2.4;',
                'Authorization': f"Token {os.environ['NETBOX_TOKEN']}"}
ip_address = '192.0.167.168/32'
address_id = input('IP address ID from 11.4b: ')

# // Disable SSL certificate warnings (unsigned certificate on server) //
requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

# // POST IP address to netbox //
data = {'address': ip_address, 'description': 'Ex 11.5 RESTful exercise'}
url = f'{rest_url}{address_id}/'
print(url)
post_response = requests.put(url, headers=http_headers, verify=False, 
	                          data=json.dumps(data))

# // Create dictionary from JSON data in response //
netbox_response = post_response.json()

# // Print out requirements from the netbox response //
print(f"""Adding IP Address: {ip_address}, '{post_response.reason}'\n""")
pprint(netbox_response)
# End
sys.exit()
