#! /usr/bin/env python3

''' ex11.3a - RESTful API - GET - report '''

import sys
import os
import requests
from pprint import pprint
from urllib3.exceptions import InsecureRequestWarning

rest_url = 'https://netbox.lasthop.io/api/dcim/devices/'
http_headers = {'accept': 'application/json; version=2.4;',
                'Authorization': f"Token {os.environ['NETBOX_TOKEN']}"}

# // Disable SSL certificate warnings (unsigned certificate on server) //
requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

# Get top level endpoints
response = requests.get(rest_url, headers=http_headers, verify=False)

# // Print out requirements from response data //
device_data = response.json()

# // Print list of devices //
devices = [x['display_name'] for x in device_data['results']]

print(devices)

# End
sys.exit()

