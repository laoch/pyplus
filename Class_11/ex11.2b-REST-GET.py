#! /usr/bin/env python3

''' ex11.2b - RESTful API - GET '''

import sys
import requests
from pprint import pprint
from urllib3.exceptions import InsecureRequestWarning

rest_url = 'https://netbox.lasthop.io/api/'
http_headers = {'accept': 'application/json; version=2.4;'}

# // Disable SSL certificate warnings (unsigned certificate on server) //
requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

# Get top level endpoints
response = requests.get(rest_url, headers=http_headers, verify=False)

# // Print out requirements from response data //
print('\nResponse Code: ')
print(response.status_code)
print('\nResponse Text: ')
print(response.text)
print('\nResponse JSON: ')
pprint(response.json())
print('\nResponse Headers: ')
pprint(dict(response.headers))

# End
sys.exit()