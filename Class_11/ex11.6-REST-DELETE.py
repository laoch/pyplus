#! /usr/bin/env python3

''' ex11.6 - RESTful API - DELETE '''

import sys
import os
import requests
import json
from pprint import pprint
from urllib3.exceptions import InsecureRequestWarning

rest_url = 'https://netbox.lasthop.io/api/ipam/ip-addresses/'
http_headers = {'Content-Type': 'application/json; version=2.4;',
				'accept': 'application/json; version=2.4;',
                'Authorization': f"Token {os.environ['NETBOX_TOKEN']}"}
address_id = input('IP address ID from 11.4b: ')

# // Disable SSL certificate warnings (unsigned certificate on server) //
requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

# // POST IP address to netbox //
url = f'{rest_url}{address_id}/'
print(url)
post_response = requests.delete(url, headers=http_headers, verify=False)

# // Print out requirements from the netbox response //
print(f"""Deleting ID: {address_id}, '{post_response.reason}'\n""")

# End
sys.exit()
