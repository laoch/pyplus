#! /usr/bin/env python3

''' ex11.4a - RESTful API - POST '''

import sys
import os
import requests
import json
from pprint import pprint
from urllib3.exceptions import InsecureRequestWarning

rest_url = 'https://netbox.lasthop.io/api/ipam/ip-addresses/'
http_headers = {'Content-Type': 'application/json; version=2.4;',
				'accept': 'application/json; version=2.4;',
                'Authorization': f"Token {os.environ['NETBOX_TOKEN']}"}
ip_address = '192.0.167.168/32'

# // Disable SSL certificate warnings (unsigned certificate on server) //
requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

# // POST IP address to netbox //
response = requests.post(rest_url, headers=http_headers, 
	                    verify=False, data=json.dumps({'address': ip_address}))

# // Create dictionary from JSON data in response //
netbox_response = response.json()

# // Print out requirements from the netbox response //
print(f"""Adding IP Address: {ip_address}, '{response.reason}'\n""")
pprint(netbox_response)

# End
sys.exit()
