#! /usr/bin/env python3

''' ex11.4b - RESTful API - POST '''

import sys
import os
import requests
import json
from pprint import pprint
from urllib3.exceptions import InsecureRequestWarning

rest_url = 'https://netbox.lasthop.io/api/ipam/ip-addresses/'
http_headers = {'Content-Type': 'application/json; version=2.4;',
				'accept': 'application/json; version=2.4;',
                'Authorization': f"Token {os.environ['NETBOX_TOKEN']}"}
ip_address = '192.0.167.168/32'

# // Disable SSL certificate warnings (unsigned certificate on server) //
requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

# // POST IP address to netbox //
post_response = requests.post(rest_url, headers=http_headers, verify=False, 
	                          data=json.dumps({'address': ip_address}))

# // Create dictionary from JSON data in response //
netbox_response = post_response.json()

# // Extract the address ID //
address_id = netbox_response['id']

# // Print out requirements from the netbox response //
print(f"""Adding IP Address: {ip_address}, '{post_response.reason}'\n""")

# // GET information on IP Address by URL //
url = f'{rest_url}{address_id}/'
print(url)
get_response = requests.get(url, headers=http_headers, verify=False)

# // Create dictionary from JSON data in response //
netbox_response = post_response.json()
pprint(netbox_response)

# End
sys.exit()
