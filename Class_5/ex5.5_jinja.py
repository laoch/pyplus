#! /usr/bin/env python3

''' Ex 5.4 jinja for loop '''

from jinja2 import FileSystemLoader, StrictUndefined
from jinja2.environment import Environment

templatef = 'ex5.5_jinja.j2'

# // Create Jinja2 Environment instance //
env = Environment(undefined=StrictUndefined)
env.loader = FileSystemLoader('.')   # Current directory

# // Get a template object  //
j2_template = env.get_template(templatef)

# // Render the object  //
out = j2_template.render()

# // Print output //
print(out)

# // End // 
