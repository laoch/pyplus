#! /usr/bin/env python3

''' Ex 5.3 jinja conditionals '''

from jinja2 import FileSystemLoader, StrictUndefined
from jinja2.environment import Environment

dict_ = {
		 'vrf_name': 'blue',
		 'rd_number': '100:1',
		 'ipv4_en': True,
         'ipv6_en': True}

print(f"{dict_}\n")

templatef = 'ex5.3_jinja.j2'

# // Create Jinja2 Environment instance //
env = Environment(undefined=StrictUndefined)
env.loader = FileSystemLoader('.')   # Current directory

# // Get a template object  //
j2_template = env.get_template(templatef)

# // Render the object with a dictionary //
out = j2_template.render(**dict_)

# // Print output //
print(out)

# // End // 
