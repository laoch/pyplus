#! /usr/bin/env python3

import textfsm
from pprint import pprint
from sh_int_status import DATA_STR

#//Template //
template = 'ex4.2-sh_int_status.template'

structured_list = list()

# // Extract raw CLI data //
re_table = textfsm.TextFSM(open(template))

# // Get header details // 
values = re_table.header

# // Parse and generate structured data //
for intf in re_table.ParseText(DATA_STR):
	dict_ = dict()
	for count, val in enumerate(intf):
		dict_[values[count]] = val
	structured_list.append(dict_)

# // Print output //
pprint(structured_list)
print()
