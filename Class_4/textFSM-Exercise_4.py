#! /usr/bin/env python3

import textfsm
from sh_int_status import DATA_STR
#from sh_int_E_2_1 import DATA_STR
#from sh_arp_srx import DATA_STR
#from sh_lldp_ne import DATA_STR
#from sh_ip_bgp_sum import DATA_STR

#//Templates //
#template = 'ex4.1-sh_int_status.template'
template = 'ex4.2-sh_int_status.template'
#template = 'ex4.3-sh_int_E2_1.template'
#template = 'ex4.4-sh_arp_srx.template'
#template = 'ex4.5-sh_lldp_ne.template'
#template = 'ex4.6-sh_ip_bgp_sum.template'

# // Extract raw CLI data //
re_table = textfsm.TextFSM(open(template))

# // Parse and generate structured data //
structured_data = re_table.ParseText(DATA_STR)

# // Add header values //
structured_data.insert(0, re_table.header)

# // Print output //
print('\nFSM Table:')
[print(x) for x in structured_data]
print()
