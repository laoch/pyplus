#! /usr/bin/env python3

""" Netmiko Exercise #1 - Connect to Cisco NXOS devices"""

import sys
import yaml   # Ensure pip3 install pyyaml
from netmiko import ConnectHandler


yaml_file = '/home/dobriain/PyNet_devices.yaml'
out_file = 'out_file.txt'
log_file = 'log_file.txt'
cisco_dict = {'device_type': 'cisco_nxos', 'session_log': log_file}
cisco_keys = ['device_type', 'host', 'username','password',
	          'secret','port', 'session_log']
# // Get YAML Data //
with open(yaml_file) as fh:
	net_dict = yaml.load(fh, Loader=yaml.FullLoader)

# // Get list of Cisco devices //
devices = list(net_dict.keys())
cisco_devices_nxos = [device for device in devices if 'nxos' in device]
 
# // Loop through all Cisco devices //
print()
for device in cisco_devices_nxos:
	net_dict[device]['port'] = net_dict[device].pop('ssh_port')
	for key in list(net_dict[device].keys()):
		if key.strip() not in cisco_keys:
			net_dict[device].pop(key)
	net_dict[device].update(cisco_dict)

	# // Connect to each and get the prompt //
	with ConnectHandler(**net_dict[device]) as netc:
		print(f"{device} prompt: {netc.find_prompt()}\n")
		if device == cisco_devices_nxos[-1]:
			print(f"'show version' from {device} sent to '{device}_{out_file}'\n")
			sh_ver = netc.send_command('show version')

# // Output show version data to file //
with open(f'{device}_{out_file}', mode='w', encoding='utf-8') as fh:
	fh.write('\n')
	fh.write(sh_ver)
	fh.write('\n')	

sys.exit(0)
