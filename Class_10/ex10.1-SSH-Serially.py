#! /usr/bin/env python3

''' Exercise #10.1 - Serially SSH '''

import sys
import pyplus_ob
from pprint import pprint
import time
from netmiko import ConnectHandler

yaml_file = '/home/dobriain/PyNet_devices.yaml'
dev_params = {'cisco3': {'device_type': 'cisco_ios'}, 
              'arista1': {'device_type': 'arista_eos', 'global_delay_factor': 4}, 
              'arista2': {'device_type': 'arista_eos', 'global_delay_factor': 4}, 
              'srx2': {'device_type': 'juniper_junos'}}
ssh_key_tup = ('host', 'port', 'username', 'password')
dev_dict = dict()
command = 'show version'

# // Instantiate the 'pyplusOb' class //
py_plus = pyplus_ob.pyplusOb()

# ---------------------------- #
#          Functions           #
# ---------------------------- #

def ssh_cmd(device, cmd):
    ''' SSH to device and execute command '''

    dh = ConnectHandler(**device)
    out = dh.send_command(cmd)
    dh.disconnect()
    return(out)

def main():

    # // Start time //
    start_time = time.time()

    # // Get device data and update it as necessary //
    print()
    for device in dev_params.keys():
        dict_ = dict()
        dict_ = py_plus.get_device_data(device)
        dict_[device]['port'] = dict_[device].pop('ssh_port')
        dict_[device] = {d: dict_[device][d] for d in ssh_key_tup}
        dict_[device].update(dev_params[device])

        # // Print the version of each device //
        line_len = (len(device) + len(command) + 4)
        print(f"'{device}': {command}")
        print('-' * line_len)
        ssh_cmd(dict_[device], command)
        dev_dict.update(dict_)

    end_time = time.time()

    print(f'\n{end_time - start_time} seconds to complete task')
    print()

    return 0

if __name__ == '__main__':
    main()


# // End //
sys.exit()

