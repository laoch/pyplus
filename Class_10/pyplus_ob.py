#! /usr/bin/env python3
# -*- coding: utf-8 -*-

''' Common functions for PyPlus course '''

# ============================================ #
# //             Module imports             // #
# ============================================ #

import sys
import yaml
from netmiko import ConnectHandler
from pprint import pprint

# ============================================ #
# //      Global variable declarations      // #
# ============================================ #

__author__ = "Diarmuid O'Briain"
__copyright__ = "Copyright 2021, C2S"
__licence__ = "European Union Public Licence v1.2"
__version__ = "1.1"


# ############################################# #
# //              pyplusOb class             // #
# ############################################# #

class pyplusOb:
    ''' pyplusOb Class '''

    # -------------------------------------------- #
    # //           Constructor Methods          // #
    # -------------------------------------------- #

    def __init__(self):
        ''' pyplusOb class constructor method '''

        self.functionality = 'Useful Pyplus methods'
        self.yaml_file = '/home/dobriain/PyNet_devices.yaml'
        self.arista_yaml_file = '/home/dobriain/PyNet_arista_devices.yaml'
        self.log_file = 'pyplus_ob.log'

    def __str__(self):
        ''' pyplusOb class __str__ method ''' 

        return (self.functionality)

    def __repr__(self):
        ''' pyplusOb class __repr__ method ''' 

        return (f'{self.__class__.__name__}'
                f' class: {self.functionality}')    


    # -------------------------------------------- #
    # //       get_device_data() Method         // #
    # -------------------------------------------- #

    def get_device_data(self, device):
        ''' Get data method '''

        dict_ = dict()
        dict2_ = dict()

        # // Get YAML Data //
        try:
            with open(self.yaml_file) as fh: 
                dict_ = yaml.load(fh, Loader=yaml.FullLoader)
        except FileNotFoundError:
            print(f'{self.yaml_file} not found') 
            sys.exit(1)

        # // Get list and dictionary of devices //
        if not device in dict_.keys():
            print(f"Error: No device '{device}' in pool, skipping")
            return({})
        else:
            dict2_[device] = dict_[device]

        return(dict2_)

    # -------------------------------------------- #
    # //             ssh_cmd() Method           // #
    # -------------------------------------------- #

    def ssh_cmd(self, device, cmd):
        ''' SSH to device and execute command methd '''

        dev_params = {'cisco3': {'device_type': 'cisco_ios'}, 
                      'arista1': {'device_type': 'arista_eos', 'global_delay_factor': 4}, 
                      'arista2': {'device_type': 'arista_eos', 'global_delay_factor': 4}, 
                      'srx2': {'device_type': 'juniper_junos'}}
        ssh_key_tup = ('host', 'port', 'username', 'password')

        # // Prepare dictionary for SSH //
        dn = list(device.keys())[0]
        device[dn]['port'] = device[dn].pop('ssh_port')
        device[dn] = {d: device[dn][d] for d in ssh_key_tup}
        device[dn].update(dev_params[dn])
        
        # // Connect to device and run command // 
        dh = ConnectHandler(**device[dn])
        cmd_out = dh.send_command(cmd)
        dh.disconnect()

        # // Print the result for the the device //
        line_len = (len(dn) + len(cmd) + 4)
        print(f"'{dn}': {cmd}")
        print('-' * line_len)
        #print(cmd_out)

        return 0

    # -------------------------------------------- #
    # //            ssh_cmd2() Method           // #
    # -------------------------------------------- #

    def ssh_cmd2(self, device, cmd):
        ''' SSH to device and execute command methd '''

        dev_params = {'cisco3': {'device_type': 'cisco_ios'}, 
                      'arista1': {'device_type': 'arista_eos', 'global_delay_factor': 4}, 
                      'arista2': {'device_type': 'arista_eos', 'global_delay_factor': 4}, 
                      'srx2': {'device_type': 'juniper_junos'}}
        ssh_key_tup = ('host', 'port', 'username', 'password')

        # // Prepare dictionary for SSH //
        dn = list(device.keys())[0]
        device[dn]['port'] = device[dn].pop('ssh_port')
        device[dn] = {d: device[dn][d] for d in ssh_key_tup}
        device[dn].update(dev_params[dn])
        
        # // Connect to device and run command // 
        dh = ConnectHandler(**device[dn])
        cmd_out = dh.send_command(cmd)
        dh.disconnect()

        # // Print the result for the the device //
        str_ = str()
        line_len = (len(dn) + len(cmd) + 4)
        hd = f"'{dn}': {cmd}\n{'-' * line_len}"
        return(hd, cmd_out)


# ######### // End pyplusOb class // ######### #

# -------------------------------------------- #
# //             main() Function            // #
# -------------------------------------------- #

def main():
    ''' main() function placeholder '''

    print('This is a module for the PyPlus course.')

    return(1)

# ============================================ #
# //                  Global                // #
# ============================================ #

# // Call main function  // 
if __name__ == "__main__":
    main()
