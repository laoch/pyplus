#! /usr/bin/env python3

''' Exercise #10.4/5 - Concurrent Futures - Processes '''

import sys
import pyplus_ob
from pprint import pprint
import time
from concurrent.futures import ProcessPoolExecutor, as_completed

yaml_file = '/home/dobriain/PyNet_devices.yaml'
dev_tup = ('cisco3', 'arista1', 'arista2',  'srx2')
command = 'show version'
dev_list = list()

# // Instantiate the 'pyplusOb' class //
py_plus = pyplus_ob.pyplusOb()

# ---------------------------- #
#          Functions           #
# ---------------------------- #

def main():

    # // Define maximum size of thread pool //
    max_processes = 4

    # // Generate list of 'show version' commands for each device //
    cmds = [command] * len(dev_tup)

    # // Start time //
    start_time = time.time()

    # // Get device data //
    for device in dev_tup:
        dev_list.append(py_plus.get_device_data(device))

    # // Context Manager //
    with ProcessPoolExecutor(max_processes) as pool:
        results_generator = pool.map(py_plus.ssh_cmd2, dev_list, cmds)

        # // Results Generator //
        for result in results_generator:
            print(result[0])
            #print(result[1])

        end_time = time.time()
        print(f'\n{end_time - start_time} seconds to complete task')
        print()

    return 0

if __name__ == '__main__':
    main()


# // End //
sys.exit()

