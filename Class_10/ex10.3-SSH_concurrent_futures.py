#! /usr/bin/env python3

''' Exercise #10.3 - Concurrent Futures '''

import sys
import pyplus_ob
from pprint import pprint
import time
from concurrent.futures import ThreadPoolExecutor, wait

yaml_file = '/home/dobriain/PyNet_devices.yaml'
dev_tup = ('cisco3', 'arista1', 'arista2',  'srx2')
command = 'show version'

# // Instantiate the 'pyplusOb' class //
py_plus = pyplus_ob.pyplusOb()

# ---------------------------- #
#          Functions           #
# ---------------------------- #

def main():

    # // Define maximum size of thread pool //
    max_threads = 4
    pool = ThreadPoolExecutor(max_threads)

    # // Start time //
    start_time = time.time()

    # // Thread container list //
    future_list = list()

    # // Get device data and establish threads //
    print()
    for device in dev_tup:
        dict_ = dict()
        dict_ = py_plus.get_device_data(device)
        future = pool.submit(py_plus.ssh_cmd, dict_, command)
        future_list.append(future)

    # // Wait until all futures complete //
    wait(future_list)   

    for future in future_list:
        print(f'Result: {future.result()}')

    end_time = time.time()

    print(f'\n{end_time - start_time} seconds to complete task')
    print()

    return 0

if __name__ == '__main__':
    main()


# // End //
sys.exit()

