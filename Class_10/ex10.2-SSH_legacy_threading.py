#! /usr/bin/env python3

''' Exercise #10.2 - Legacy threading '''

import sys
import pyplus_ob
from pprint import pprint
import time
import threading

yaml_file = '/home/dobriain/PyNet_devices.yaml'
dev_tup = ('cisco3', 'arista1', 'arista2',  'srx2')
command = 'show version'

# // Instantiate the 'pyplusOb' class //
py_plus = pyplus_ob.pyplusOb()

# ---------------------------- #
#          Functions           #
# ---------------------------- #

def main():

    # // Start time //
    start_time = time.time()

    # // Get device data and establish threads //
    print()
    for device in dev_tup:
        dict_ = dict()
        dict_ = py_plus.get_device_data(device)
        threading.Thread(target=py_plus.ssh_cmd, args=(dict_, command)).start()

    # // Iterate over the threads and wait for end //
    for th in threading.enumerate():       # All threads
        if th != threading.main_thread():  # Exclude the main thread
            #print(th)
            th.join()    

    end_time = time.time()

    print(f'\n{end_time - start_time} seconds to complete task')
    print()

    return 0

if __name__ == '__main__':
    main()


# // End //
sys.exit()

