#! /usr/bin/env python3
''' ex12.5b '''

def test_prompt(netmiko_connect):        # Decorated fun added as parameter
    ''' Test netmiko 'find_prompt' '''
    prompt = netmiko_connect.find_prompt()
    assert prompt == f'arista1#'
    return 0

def test_show_version(netmiko_connect):  # Decorated fun added as parameter
    ''' Test netmiko 'show_version' '''
    sh_ver = netmiko_connect.send_command("show version")
    assert 'Software image version: 4.20.10M' in sh_ver
    return 0

