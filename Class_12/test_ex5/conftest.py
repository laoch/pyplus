#! /usr/bin/env python3
''' ex 12.5a '''

import os
import pytest
import my_devices 
from getpass import getpass 
from netmiko import ConnectHandler

@pytest.fixture(scope="module")   # Added Decorator
def netmiko_connect(request):
    ''' Connect to arista1 and return connection object '''

    # // Adjust 'arista1' dictionary //
    arista = dict(**my_devices.arista1)
    arista['host'] = arista.pop('hostname')
    arista['device_type'] = 'arista_eos'
    del(arista['platform'])

    # // Connection //
    nch = ConnectHandler(**arista)

    def fin():
        nch.disconnect()

    request.addfinalizer(fin)
    return nch
    

