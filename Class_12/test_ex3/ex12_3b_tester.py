#! /usr/bin/env python3
''' ex12.3b '''

import ex12_3a_test_simple as test


def test_my_add():
    ''' Test the 'my_ad' function '''
    assert test.my_add(5, 11) == 16
    assert test.my_add(1, 9) == 10
    return 0


def test_my_mul():
    ''' Test the 'my_mul' function '''
    assert test.my_mul(2, 7) == 14
    assert test.my_mul(9, 9) == 81
    return 0


def main():
    ''' main() function (Tests) '''
    if test_my_add() == 0:
    	print("Test 'my_add' is good")

    if test_my_mul() == 0:
    	print("Test 'my_mul' is good")

    return 0

# // Global space //
if __name__ == '__main__':
    main()
