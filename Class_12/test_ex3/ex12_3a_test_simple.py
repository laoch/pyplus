#! /usr/bin/env python3
''' ex12.3a '''


def my_add(x, y):
    ''' Add two integers together'''
    return x + y


def my_mul(x, y):
    ''' Multiply two integers together '''
    return x * y
