#!/usr/bin/env python3
''' ex12.4a '''

import os
import my_devices
from netmiko import ConnectHandler


def netmiko_connect():
    ''' Connect to arista1 and return connection object '''

    # // Adjust 'arista1' dictionary //
    arista = dict(**my_devices.arista1)
    arista['host'] = arista.pop('hostname')
    arista['device_type'] = 'arista_eos'
    del(arista['platform'])

    # // Connection //
    nch = ConnectHandler(**arista)
    return nch
    

def main():
    ''' main() function '''

    # // Netmiko connection object //
    print(netmiko_connect())


# // Global space //
if __name__ == '__main__':
    main()