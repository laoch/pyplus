#!/usr/bin/env python3
''' ex12.4c '''

import os
import pytest
import my_devices 
from netmiko import ConnectHandler

@pytest.fixture(scope="module")   # Added Decorator
def netmiko_connect():
    ''' Connect to arista1 and return connection object '''

    # // Adjust 'arista1' dictionary //
    arista = dict(**my_devices.arista1)
    arista['host'] = arista.pop('hostname')
    arista['device_type'] = 'arista_eos'
    del(arista['platform'])

    # // Connection //
    nch = ConnectHandler(**arista)
    return nch
    

def test_prompt(netmiko_connect):        # Decorated fun added as parameter
    ''' Test netmiko 'find_prompt' '''
    prompt = netmiko_connect.find_prompt()
    assert prompt == f'arista1#'
    return 0


def test_show_version(netmiko_connect):  # Decorated fun added as parameter
    ''' Test netmiko 'show_version' '''
    sh_ver = netmiko_connect.send_command("show version")
    assert 'Software image version: 4.20.10M' in sh_ver
    return 0


def main():
    ''' main() function '''

    # // Tests //
    if test_prompt() == 0:
        print('Test prompt: Passed')

    if test_show_version() == 0:
        print('show version: Passed')


# // Global space //
if __name__ == '__main__':
    main()