#!/usr/bin/env python3
''' ex12.4c '''

import os
import my_devices 
from netmiko import ConnectHandler


def netmiko_connect():
    ''' Connect to arista1 and return connection object '''

    # // Adjust 'arista1' dictionary //
    arista = dict(**my_devices.arista1)
    arista['host'] = arista.pop('hostname')
    arista['device_type'] = 'arista_eos'
    del(arista['platform'])

    # // Connection //
    nch = ConnectHandler(**arista)
    return nch
    

def test_prompt():
    ''' Test netmiko 'find_prompt' '''
    neth = netmiko_connect()
    prompt = neth.find_prompt()
    assert prompt == f'arista1#'
    return 0


def test_show_version():
    ''' Test netmiko 'show_version' '''
    neth = netmiko_connect()
    assert 'Software image version: 4.20.10M' in neth.send_command("show version")
    return 0


def main():
    ''' main() function '''

    # // Tests //
    if test_prompt() == 0:
        print('Test prompt: Passed')

    if test_show_version() == 0:
        print('show version: Passed')


# // Global space //
if __name__ == '__main__':
    main()