#! /usr/bin/env python3
# -*- coding: utf-8 -*-

''' Ex 6.2a Arista eAPI template '''

# ============================================ #
# //             Module imports             // #
# ============================================ #

import sys
import pyplus_ob
import pyeapi
from pprint import pprint

# ============================================ #
# //      Global variable declarations      // #
# ============================================ #

__author__ = "Diarmuid O'Briain"
__copyright__ = "Copyright 2021, C2S"
__licence__ = "European Union Public Licence v1.2"
__version__ = "1.1"

device = 'arista4'
command = 'show ip arp'

# -------------------------------------------- #
# //      arista_eapi_show() Function       // #
# -------------------------------------------- #

def arista_eapi_show(cmd, kwargs):
    ''' Arista eAPI connection '''

    # // Connection object to Arista switch //
    con = pyeapi.client.connect(**kwargs)

    # // Create Node object //
    arista_dev = pyeapi.client.Node(con)

    return(arista_dev.enable(cmd))

# -------------------------------------------- #
# //             main() Function            // #
# -------------------------------------------- #

def main():
    ''' main() function '''

    # // Instantiate the 'pyplusOb' class //
    py_plus = pyplus_ob.pyplusOb()

    # // Get data on arista3 //
    _ = py_plus.get_device_data(device)
    arista = (py_plus.arista_eapi(_[device]))

    # // Call the arista_sh_ip_arp function //
    out = arista_eapi_show(command, arista)

    # // Print IP ARP table //
    arp_tbl = out[0]['result']['ipV4Neighbors']
    print(f"\nMAC Address table from {device}")
    print('\nMAC Address:\tIP Address\n')
    for arp in arp_tbl:
        print(f"{arp['hwAddress']}:\t{arp['address']}")
    print()


    return(0)

# ============================================ #
# //                  Global                // #
# ============================================ #

# // Call main function  // 
if __name__ == "__main__":
    main()
else:
    sys.exit(1)

# // End //
#sys.exit(0)
