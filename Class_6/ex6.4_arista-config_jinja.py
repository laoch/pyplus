#! /usr/bin/env python3
# -*- coding: utf-8 -*-

''' Ex 6.4 Arista eAPI template '''

# ============================================ #
# //             Module imports             // #
# ============================================ #

import sys
import pyplus_ob
from jinja2 import FileSystemLoader, StrictUndefined
from jinja2.environment import Environment

templatef = 'ex6.4_arista_config.j2'

# ============================================ #
# //      Global variable declarations      // #
# ============================================ #

__author__ = "Diarmuid O'Briain"
__copyright__ = "Copyright 2021, C2S"
__licence__ = "European Union Public Licence v1.2"
__version__ = "1.1"

devices = ['arista1', 'arista2', 'arista3', 'arista4']
command = 'show ip interface brief'

# -------------------------------------------- #
# //             main() Function            // #
# -------------------------------------------- #

def main():
    ''' main() function '''

    arista_keys = ['host', 'username', 'password', 'transport']

    # // Instantiate the 'pyplusOb' class //
    py_plus = pyplus_ob.pyplusOb()

    # // Create Jinja2 Environment instance //
    env = Environment(undefined=StrictUndefined)
    env.loader = FileSystemLoader('.')   # Current directory

    # // Get a template object  //
    j2_template = env.get_template(templatef)

    # // Get data on arista devices //
    print()
    for device in devices:

        dict_ = dict()
        arista = py_plus.get_arista_device_data(device)
        dict_ = {d: arista[device][d] for d in arista_keys}
        data_dict = arista[device]['data']

        # // Render the object from the j2 template //
        j2_out = j2_template.render(**data_dict)

        # // Split commands into a list of items //
        j2_out_list = j2_out.split('\n')

        # // Call the py_plus_ob arista_eapi_config Method //
        out = py_plus.arista_eapi_config(j2_out_list, dict_)

        # // Call the py_plus_ob arista_eapi_show Method //
        if out == [{}, {}]:
            out_show = py_plus.arista_eapi_show(command, dict_)
            print(f"Device [{device}]: 'show ip interface brief'\n")
            print(out_show[0]['result']['output'])

    return(0)

# ============================================ #
# //                  Global                // #
# ============================================ #

# // Call main function  // 
if __name__ == "__main__":
    main()
else:
    sys.exit(1)

# // End //
#sys.exit(0)
