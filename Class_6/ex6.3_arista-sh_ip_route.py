#! /usr/bin/env python3
# -*- coding: utf-8 -*-

''' Ex 6.3 Arista eAPI template '''

# ============================================ #
# //             Module imports             // #
# ============================================ #

import sys
import pyplus_ob
from pprint import pprint

# ============================================ #
# //      Global variable declarations      // #
# ============================================ #

__author__ = "Diarmuid O'Briain"
__copyright__ = "Copyright 2021, C2S"
__licence__ = "European Union Public Licence v1.2"
__version__ = "1.1"

device = 'arista4'
command = 'show ip route'



# -------------------------------------------- #
# //             main() Function            // #
# -------------------------------------------- #

def main():
    ''' main() function '''

    # // Instantiate the 'pyplusOb' class //
    py_plus = pyplus_ob.pyplusOb()

    # // Get data on arista4 //
    _ = py_plus.get_device_data(device)
    arista = (py_plus.arista_eapi(_[device]))

    # // Call the py_plus_ob arista_sh_ip_route Method //
    out = py_plus.arista_eapi_show(command, arista)
    list_ = py_plus.arista_ip_route_table(device, out)

    print(*list_, sep='\n')
    print()

    return(0)

# ============================================ #
# //                  Global                // #
# ============================================ #

# // Call main function  // 
if __name__ == "__main__":
    main()
else:
    sys.exit(1)

# // End //
#sys.exit(0)
