#! /usr/bin/env python3

""" Netmiko Exercise #2.2 - Connect to Cisco device (expect string)"""

# ============================================ #
# //             Module imports             // #
# ============================================ #

import sys
from netmiko import ConnectHandler
import pyplus

# ============================================ #
# //      Global variable declarations      // #
# ============================================ #

__author__ = "Diarmuid O'Briain"
__program__ = sys.argv[0][2:] if (sys.argv[0][:2] == './') else sys.argv[0]
__version__ = '1.1'

yaml_file = '/home/dobriain/PyNet_devices.yaml'
#yaml_file = '../basic_local.yaml'
dev_list = ['cisco1', 'cisco3', 'cisco4']
dev_dict = dict()
log_file = 'log_file.txt'
ping_ip = '8.8.8.8'

# ============================================ #
# //                Functions               // #
# ============================================ #

# -------------------------------------------- #
# //           get_data() Function          // #
# -------------------------------------------- #


def get_data():
    ''' Get data from user '''

    print('\nRespond to the following questions, please:\n')

    pingp = {'Protocol': 'ip', 
             'Target IP address': ping_ip, 
             'Repeat count': '5',
             'Datagram size': '100',
             'Timeout in seconds': '2',
             'Extended commands': 'n',
             'Sweep range of sizes': 'n'}

    # // Get input from the user //
    for key, value in pingp.items():
        str_ = input(f'{key} [{value}]: ')
        pingp[key] = str_ if (str_ != '') else pingp[key]

    return(pingp)

# -------------------------------------------- #
# //          cisco_ping() Function         // #
# -------------------------------------------- #


def cisco_ping(pingp, kwargs):
    ''' Carry out remote ping on Cisco device '''

    expect_list = list(pingp.keys())
    expect_list.append('#')
    send_list = ['ping', *list(pingp.values())]
    len_ = len(send_list) -1
    count = list(range(0, len_))

    # // Connect to each device, send commands and get responses //
    with ConnectHandler(**kwargs) as netc:
        for c in count:
            netc.send_command(send_list[c], expect_string=expect_list[c])
        out = netc.send_command(send_list[len_], expect_string=expect_list[len_])

    return(out)


# -------------------------------------------- #
# //               main() Function          // #
# -------------------------------------------- #


def main():
    ''' main() function '''

    print()
    # // Get device data //
    for device in dev_list:
        dev_dict.update(pyplus.get_device_data(device, yaml_file))

    # // Printing devices that can be worked on //
    dev_str = ', '.join(dev_dict.keys())
    print(f'Working on devices: {dev_str}')

    # // Get data from the user //
    pingp = get_data()

    # // Working on each 'Cisco' device in turn //
    for device in dev_dict.keys():
        dict_ = pyplus.cisco_ios(dev_dict[device], log_file)
        output = cisco_ping(pingp, dict_)
        print(f"\n=== {device} ===\n{output}\n")

  
# ============================================ #
# //                  Global                // #
# ===========================##=============== #

# // Call main function // #

if __name__ == "__main__":
    main()

# //End //
sys.exit()
