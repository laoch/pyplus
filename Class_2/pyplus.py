""" Netmiko PyPlus module """

# ============================================ #
# //             Module imports             // #
# ============================================ #

import yaml   # Ensure pip3 install pyyaml

# ============================================ #
# //                Functions               // #
# ============================================ #

# -------------------------------------------- #
# //           get_data() Function          // #
# -------------------------------------------- #


def get_device_data(device, yaml_file):
    ''' Get device data from YAML file '''
    dict_ = dict()
    dict2_ = dict()

    # // Get YAML Data //
    with open(yaml_file) as fh:
        dict_ = yaml.load(fh, Loader=yaml.FullLoader)

    # // Get list and dictionary of Cisco devices //
    if not device in dict_.keys():
        print(f"Error: No device '{device}' in pool, skipping")
        return({})
    else:
        dict2_[device] = dict_[device]

    return(dict2_)

# -------------------------------------------- #
# //           cisco_ios() Function         // #
# -------------------------------------------- #


def cisco_ios(kwargs, log_file='log_file.txt'):
    ''' Sort key/value pairs for Cisco IOS devices '''

    cisco_ios_dict = {'device_type': 'cisco_ios', 
                      'session_log': log_file}
    cisco_keys = ('device_type', 'host', 'username',
                  'password', 'port', 'session_log')

    # // Add cisco_ios_dict //
    kwargs = dict(kwargs, **cisco_ios_dict)

    # // Switch ssh_port for port //
    kwargs['port'] = kwargs.pop('ssh_port')

    # // Remove unnecessary values
    kwargs = {d: kwargs[d] for d in cisco_keys}
    
    return(kwargs)

 # -------------------------------------------- #
# //          cisco_nxos() Function         // #
# -------------------------------------------- #


def cisco_nxos(kwargs, log_file='log_file.txt'):
    ''' Sort key/value pairs for Cisco IOS devices '''

    cisco_ios_dict = {'device_type': 'cisco_nxos', 
                      'session_log': log_file}
    cisco_keys = ('device_type', 'host', 'username',
                  'password', 'port', 'session_log')

    # // Add cisco_ios_dict //
    kwargs = dict(kwargs, **cisco_ios_dict)

    # // Switch ssh_port for port //
    kwargs['port'] = kwargs.pop('ssh_port')

    # // Remove unnecessary values
    kwargs = {d: kwargs[d] for d in cisco_keys}
    
    return(kwargs)