#! /usr/bin/env python3

""" Netmiko Exercise #2.3 - Connect to Cisco device (Delay Factor)"""

# ============================================ #
# //             Module imports             // #
# ============================================ #

import sys
import datetime
from netmiko import ConnectHandler
import pyplus

# ============================================ #
# //      Global variable declarations      // #
# ============================================ #

__author__ = "Diarmuid O'Briain"
__program__ = sys.argv[0][2:] if (sys.argv[0][:2] == './') else sys.argv[0]
__version__ = '1.1'

yaml_file = '/home/dobriain/PyNet_devices.yaml'
#yaml_file = '../basic_local.yaml'
dev_str = 'nxos2'
dev_dict = dict()
log_file = 'log_file.txt'


# ============================================ #
# //                Functions               // #
# ============================================ #

# -------------------------------------------- #
# //               main() Function          // #
# -------------------------------------------- #


def main():
    ''' main() function '''

    print()

    # // Printing the device be worked on //
    print(f'Working on device: {dev_str}\n')

    # // Get device data //
    dev_dict = pyplus.get_device_data(dev_str, yaml_file)

    # // Working on the device to clean specs //
    dict_ = pyplus.cisco_ios(dev_dict[dev_str], log_file)
    dict_.update({'global_delay_factor': 2})

    # // Connect to the device and get the prompt //
    with ConnectHandler(**dict_) as netc:
        time1 = datetime.datetime.now().strftime("%I:%M:%S.%f")
        out1 = netc.send_command('show lldp neighbors detail')         

        time2 = datetime.datetime.now().strftime("%I:%M:%S.%f")
        out2 = netc.send_command('show lldp neighbors detail', 
                                delay_factor=8)         
    time3 = datetime.datetime.now().strftime("%I:%M:%S.%f")

    print(f'\n{out1}\n')
    print('=' * 40)
    print(f'\n{out2}\n')
    print('=' * 40)
    print(f"Time 1: (No delay)  : {time1}\n"
          f"Time 2: (Withdelay) : {time2}\n"
          f"Time 3: (Completion): {time3}\n")
  
# ============================================ #
# //                  Global                // #
# ===========================##=============== #

# // Call main function // #

if __name__ == "__main__":
    main()

# //End //
sys.exit()
