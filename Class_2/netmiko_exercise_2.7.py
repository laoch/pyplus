#! /usr/bin/env python3

""" Netmiko Exercise #2.7 - Connect to Cisco device (Misc)"""

# ============================================ #
# //             Module imports             // #
# ============================================ #

import sys
from netmiko import ConnectHandler
from getpass import getpass
import pyplus
import time

# ============================================ #
# //      Global variable declarations      // #
# ============================================ #

__author__ = "Diarmuid O'Briain"
__program__ = sys.argv[0][2:] if (sys.argv[0][:2] == './') else sys.argv[0]
__version__ = '1.1'

yaml_file = '/home/dobriain/PyNet_devices.yaml'
#yaml_file = '../basic_local.yaml'
dev_str = 'cisco4'
dev_dict = dict()
log_file = 'log_file.txt'

# ============================================ #
# //                Functions               // #
# ============================================ #

# -------------------------------------------- #
# //               main() Function          // #
# -------------------------------------------- #


def main():
    ''' main() function '''

    print()

    # // Printing the device be worked on //
    print(f'Working on device {dev_str}\n')

    # // Get the enable secretr//
    secret = getpass(f'Enter the enable secret: ')    
    print()

    # // Get device data //
    dev_dict = pyplus.get_device_data(dev_str, yaml_file)

    # // Working on the device to clean specs //
    dict_ = pyplus.cisco_ios(dev_dict[dev_str], log_file)

    # // Get the secret from the user //
    dict_.update({'session_log': 'my_output.txt', 'secret': secret})

    # // Connect to the device and carry out instructions from ex. //
    with ConnectHandler(**dict_) as netc:
        print(netc.find_prompt())         # a.
        print(netc.config_mode())         # b.
        netc.exit_config_mode()           # c.
        print(netc.find_prompt())         # c.
        netc.write_channel('disable\n')   # d.
        time.sleep(2)                     # e.
        print(netc.read_channel())        # e.
        netc.enable()                     # f.
        print(netc.find_prompt())         # f.

        return(0)

# ============================================ #
# //                  Global                // #
# ===========================##=============== #

# // Call main function // #

if __name__ == "__main__":
    main()

# //End //
sys.exit()
