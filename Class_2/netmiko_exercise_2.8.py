cd#! /usr/bin/env python3

""" Netmiko Exercise #2.8 - Connect to Cisco device (SSH Keys) """

# ============================================ #
# //             Module imports             // #
# ============================================ #

import sys
from netmiko import ConnectHandler
from getpass import getpass
import pyplus
import time

# ============================================ #
# //      Global variable declarations      // #
# ============================================ #

__author__ = "Diarmuid O'Briain"
__program__ = sys.argv[0][2:] if (sys.argv[0][:2] == './') else sys.argv[0]
__version__ = '1.1'

yaml_file = '/home/dobriain/PyNet_devices.yaml'
#yaml_file = '../basic_local.yaml'
dev_str = 'cisco3'
dev_dict = dict()
log_file = 'log_file.txt'

# ============================================ #
# //                Functions               // #
# ============================================ #

# -------------------------------------------- #
# //               main() Function          // #
# -------------------------------------------- #


def main():
    ''' main() function '''

    print()

    # // Printing the device be worked on //
    print(f'Working on device {dev_str}\n')

    # // Get device data //
    dev_dict = pyplus.get_device_data(dev_str, yaml_file)

    # // Working on the device to clean specs //
    dict_ = pyplus.cisco_ios(dev_dict[dev_str], log_file)

    # // Update the dictionary with the SSH keys //
    dict_.update({'key_file': '/home/dobriain/.ssh/id_rsa'})
    dict_['username'] = 'testuser1'
    dict_.pop('password')
    print(f'dict_\n')

    # // Connect to the device and carry out instructions from ex. //
    with ConnectHandler(**dict_) as netc:
        print(netc.find_prompt())        
 
    return(0)

# ============================================ #
# //                  Global                // #
# ===========================##=============== #

# // Call main function // #

if __name__ == "__main__":
    main()

# //End //
sys.exit()
