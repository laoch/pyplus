#! /usr/bin/env python3

""" Netmiko Exercise #2.4 - Connect to Cisco device (textFSM)"""

# ============================================ #
# //             Module imports             // #
# ============================================ #

import sys
from netmiko import ConnectHandler
import pyplus

# ============================================ #
# //      Global variable declarations      // #
# ============================================ #

__author__ = "Diarmuid O'Briain"
__program__ = sys.argv[0][2:] if (sys.argv[0][:2] == './') else sys.argv[0]
__version__ = '1.1'

yaml_file = '/home/dobriain/PyNet_devices.yaml'
#yaml_file = '../basic_local.yaml'
dev_str = 'cisco4'
dev_dict = dict()
log_file = 'log_file.txt'
cmds = ('show version', 'show lldp neighbors')

# ============================================ #
# //                Functions               // #
# ============================================ #

# -------------------------------------------- #
# //               main() Function          // #
# -------------------------------------------- #


def main():
    ''' main() function '''

    print()

    # // Printing the device be worked on //
    print(f'Working on device: {dev_str}\n')

    # // Get device data //
    dev_dict = pyplus.get_device_data(dev_str, yaml_file)

    # // Working on the device to clean specs //
    dict_ = pyplus.cisco_ios(dev_dict[dev_str], log_file)

    list_ = list()
    # // Connect to the device and get the prompt //
    with ConnectHandler(**dict_) as netc:
        for cmd in cmds:
            list_.append(netc.send_command(cmd, use_textfsm=True))
    
    # // Print out the information //
    print(f'{list_[0]}\n')     # show version
    print(f'{list_[1]}\n')     # show lldp neighbour
    print('\nOutermost data structure: list\n')
    print(f"LLDP Neighbour interface: {list_[1][0]['neighbor_interface']}\n")  

  
# ============================================ #
# //                  Global                // #
# ===========================##=============== #

# // Call main function // #

if __name__ == "__main__":
    main()

# //End //
sys.exit()
