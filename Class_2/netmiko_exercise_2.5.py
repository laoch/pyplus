#! /usr/bin/env python3

""" Netmiko Exercise #2.5 - Connect to Cisco device (Send Config Set)"""

# ============================================ #
# //             Module imports             // #
# ============================================ #

import sys
import datetime
from netmiko import ConnectHandler
import pyplus

# ============================================ #
# //      Global variable declarations      // #
# ============================================ #

__author__ = "Diarmuid O'Briain"
__program__ = sys.argv[0][2:] if (sys.argv[0][:2] == './') else sys.argv[0]
__version__ = '1.1'

yaml_file = '/home/dobriain/PyNet_devices.yaml'
#yaml_file = '../basic_local.yaml'
dev_str = 'cisco3'
dev_dict = dict()
log_file = 'log_file.txt'
cmds = ('ip name-server 1.1.1.1',
        'ip name-server 1.0.0.1',
        'ip domain-lookup')
pinger = 'google.com'

# ============================================ #
# //                Functions               // #
# ============================================ #

# -------------------------------------------- #
# //               main() Function          // #
# -------------------------------------------- #


def main():
    ''' main() function '''

    print()

    # // Printing the device be worked on //
    print(f'Working on device {dev_str}\n')

    # // Get device data //
    dev_dict = pyplus.get_device_data(dev_str, yaml_file)

    # // Working on the device to clean specs //
    dict_ = pyplus.cisco_ios(dev_dict[dev_str], log_file)

    time = list()

    # // Connect to the device and get the prompt //
    with ConnectHandler(**dict_) as netc:
        time.append(datetime.datetime.now().strftime("%I:%M:%S.%f"))
        out = netc.send_config_set(cmds)
        time.append(datetime.datetime.now().strftime("%I:%M:%S.%f"))

    # // Add fast_cli to the dict_ //
    dict_['fast_cli'] = True

    # // Rerun with fast_cli set //
    with ConnectHandler(**dict_) as netc:
        time.append(datetime.datetime.now().strftime("%I:%M:%S.%f"))
        out2 = netc.send_config_set(cmds)
        time.append(datetime.datetime.now().strftime("%I:%M:%S.%f"))

    # // Print output //
    print(out)
    print()
    print(f'(No fast_cli_) Before time: {time[0]}')
    print(f'(No fast_cli_) After time : {time[1]}')

    print()
    print(f'(fast_cli_) Before time: {time[2]}')
    print(f'(fast_cli_) After time : {time[3]}')
    print()

    # // Remove fast_cli to the dict_ //
    dict_.pop('fast_cli')

    # // Test DNS //
    with ConnectHandler(**dict_) as netc:
        out3 = netc.send_command(f'ping {pinger}')
    print(out3)

# ============================================ #
# //                  Global                // #
# ===========================##=============== #

# // Call main function // #

if __name__ == "__main__":
    main()

# //End //
sys.exit()
