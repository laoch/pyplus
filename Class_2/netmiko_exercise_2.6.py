#! /usr/bin/env python3

""" Netmiko Exercise #2.6 - Connect to Cisco device (Send Config From File)"""

# ============================================ #
# //             Module imports             // #
# ============================================ #

import sys
import time
from netmiko import ConnectHandler
import pyplus

# ============================================ #
# //      Global variable declarations      // #
# ============================================ #

__author__ = "Diarmuid O'Briain"
__program__ = sys.argv[0][2:] if (sys.argv[0][:2] == './') else sys.argv[0]
__version__ = '1.1'

yaml_file = '/home/dobriain/PyNet_devices.yaml'
#yaml_file = '../basic_local.yaml'
dev_tup = ('nxos1', 'nxos2')
dev_dict = dict()
log_file = 'log_file.txt'
cmd_file = 'vlan_config.txt'


# ============================================ #
# //                Functions               // #
# ============================================ #

# -------------------------------------------- #
# //               main() Function          // #
# -------------------------------------------- #


def main():
    ''' main() function '''

    for device in dev_tup:

        list_ = list()

        print()

        # // Printing the device be worked on //
        print(f'Working on device: {device}\n')

        # // Get device data //
        dev_dict = pyplus.get_device_data(device, yaml_file)

        # // Working on the device to clean specs //
        dict_ = pyplus.cisco_nxos(dev_dict[device], log_file)

        dict_.update({'fast_cli': True})

        # // Connect to the device and send the config  //
        with ConnectHandler(**dict_) as netc:
            netc.send_config_from_file(cmd_file)
            netc.save_config()
            list_ = netc.send_command('show vlan', use_textfsm=True)

        for vlan in list_:
            if (100 <= int(vlan['vlan_id']) >= 100): 
                print(f"{vlan['vlan_id']}: Status: {vlan['status']}")

    print()

# ============================================ #
# //                  Global                // #
# ===========================##=============== #

# // Call main function // #

if __name__ == "__main__":
    main()

# //End //
sys.exit()
