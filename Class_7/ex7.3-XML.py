#! /usr/bin/env python3

''' Exercise #7.3 - XML '''

# // Use a generic function that accepts an argument //
# // "filename" to open and read a file.             //

import xmltodict

files = ['show_security_zones.xml', 
         'show_security_zones_trust.xml']

def xml_reader(file):
	''' Read XML and return xmltodict data structure '''

	# // Read the XML file as a etree //
	with open(file) as fh:
		raw_obj_data = fh.read().strip()
		xmlt_data = xmltodict.parse(raw_obj_data)

	return(xmlt_data)

def xml_reader_force_list(file, kwargs):
	''' Read XML and return xmltodict data structure '''

	# // Read the XML file as a etree //
	with open(file) as fh:
		raw_obj_data = fh.read().strip()
		xmlt_data = xmltodict.parse(raw_obj_data, force_list=kwargs)

	return(xmlt_data)

print('\nExercise #7.3a')
# // Print out this new variable and its type //
for file in files:
	print(f"\n{file}\n{'-' * len(file)}\n")
	print(xml_reader(file))

print('\nExercise #7.3b')
# // Print out this new variable and its type //
for file in files:
	print(f"{file}: ", end='')
	get_zone_sec = xml_reader(file)
	print(type(get_zone_sec['zones-information']['zones-security']))

print('\nExercise #7.3c')
# // Print out this new variable and its type using force_list //
for file in files:
	print(f"{file}: ", end='')
	get_zone_sec = xml_reader_force_list(file, {"zones-security": True})
	print(type(get_zone_sec['zones-information']['zones-security']))
"""