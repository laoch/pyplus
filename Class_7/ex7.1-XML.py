#! /usr/bin/env python3

''' Exercise #7.1 - XML '''

from lxml import etree

# // Read the XML file as a etree //
with open("show_security_zones.xml") as fh:
	sh_sec = fh.read().strip()
	sh_sec_etree = etree.fromstring(sh_sec)

print('\nExercise #7.1a')
# // Print the etree //
print(sh_sec_etree, type(sh_sec_etree))

print('\nExercise #7.1b')
# // Convert the etree object to a unicode string //
print(etree.tostring(sh_sec_etree).decode())

print('\nExercise #7.1c')
# // Print the root element tag of the etree //
print('Root element tag: ', sh_sec_etree.tag)
print('No. of children: ', len(sh_sec_etree.getchildren()))

print('\nExercise #7.1d')
# // Print the root element tag of the etree //
print('First child element: ', sh_sec_etree[0].tag)
print('First child element: ', sh_sec_etree.getchildren()[0].tag)

print('\nExercise #7.1e')
# // Create 'trust_zone' variable and assign 1st 'zones-security' //
trust_zone = sh_sec_etree[0]
print(trust_zone[0].text)

print('\nExercise #7.1f')
# // Iterate through all of the child elements of the 'trust_zone' variable //
for child in trust_zone:
	print(child.tag)