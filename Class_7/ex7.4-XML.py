#! /usr/bin/env python3

''' Exercise #7.4 - XML '''

from lxml import etree

# // Read the XML file as a etree //
with open("show_security_zones.xml") as fh:
	sh_sec = fh.read().strip()
	sh_sec_etree = etree.fromstring(sh_sec)

print('\nExercise #7.4a')

zone_sec_element = sh_sec_etree.find("zones-security")
str_ = "Find tag of the first zones-security element"
print(f"{str_}\n{'-' * len(str_)}")
print(zone_sec_element.tag)

print()
str_ = "Find tag of all child elements of the first zones-security element"
print(f"{str_}\n{'-' * len(str_)}")
for child in zone_sec_element.getchildren():
	print(child.tag)

print('\nExercise #7.4b')
zonename_element = sh_sec_etree.find(".//zones-security-zonename")
print(zonename_element.text)

print('\nExercise #7.4c')
sec_zones = sh_sec_etree.findall(".//zones-security")
for zone in sec_zones:
	print(zone.find("./zones-security-zonename").text)

