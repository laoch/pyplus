#! /usr/bin/env python3

''' Exercise #7.2 - XML '''

#// Install: python3 -m pip install xmltodict

import xmltodict

# // Read the XML file as a etree //
with open("show_security_zones.xml") as fh:
	sh_sec = fh.read().strip()
	sh_sec_etree = xmltodict.parse(sh_sec)

print('\nExercise #7.2a')
# // Print out this new variable and its type //
print(sh_sec_etree, type(sh_sec_etree))

print('\nExercise #7.2b')
zones_security = sh_sec_etree['zones-information']['zones-security']
for c, x in enumerate(zones_security):
	print(f"Security Zone #{c + 1}: {x['zones-security-zonename']}")