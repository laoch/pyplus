#! /usr/bin/env python3

''' Exercise #7.7 - NX-API '''

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from nxapi_plumbing import Device
from lxml import etree
import pyplus_ob

yaml_file = '/home/dobriain/PyNet_devices.yaml'
dev_str = 'nxos1'

# // Disable Self-signed Certificate Warnings //
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# // Instantiate the 'pyplusOb' class //
py_plus = pyplus_ob.pyplusOb()

# // Get device data //
_ = py_plus.get_device_data(dev_str)
dict_ = (py_plus.cisco_nxos(_[dev_str]))

# // Working on the device to clean specs //
dict_.update({'transport': 'https', 
              'verify': False, 
              'api_format': 'xml'})

for key in ['device_type', 'session_log']:
    dict_.pop(key)

device = Device(**dict_)

def show_xml(cmds):
    ''' Show XML from NXOS device '''

    list_ = list()
    show_cmds_all = device.show_list(cmds)
    for out in show_cmds_all:
        list_.append(etree.tostring(out, encoding="unicode"))
    return(list_)

def config_xml(cmds):
    ''' Config NXOS device via NX-API '''

    list_ = list()
    config_cmds_all = device.config_list(cmds)
    for out in config_cmds_all:
        list_.append(etree.tostring(out, encoding="unicode"))
    return(list_)

print('\nExercise #7.7a')
# // Send the "show interface Ethernet1/1" command to the device //
sh_intf_e1_1_all = device.show("show interface Ethernet1/1")

list_ = list()
dict_ = {'interface': 'Interface', 'state': 'State', 'eth_mtu': ' MTU'}
for key, value in dict_.items():
    str_ = sh_intf_e1_1_all.find(f".//{key}").text
    print(f"{value}: {str_}; ", end='')
print('\n')

print('\nExercise #7.7b')
cmds = ["show system uptime", "show system resources"]
output = show_xml(cmds)
for cmd in output: 
    print(f'{cmd}\n')

print('\nExercise #7.7c')
cmds = ['interface loopback', 
        'description loopback']
list_=list()
for lo_int in ['150', '151']:
    list2_ = list()
    list2_ = [f'{x}{lo_int}' for x in cmds]
    list2_.append('no shutdown')
    list_.extend(list2_)
for reply in config_xml(list_):
    print(reply)

