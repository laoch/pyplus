#! /usr/bin/env python3

''' Exercise #7.5 - XML '''

from lxml import etree

# // Read the XML file as a etree //
# // NOTE:  Because the document encoding is at the top of the file,   //
# // the file is read using "rb" mode (the "b" signifies binary mode). //
with open("show_version.xml", mode='rb') as fh:
	sh_ver = fh.read().strip()
	sh_ver_etree = etree.fromstring(sh_ver)

print('\nExercise #7.5a')
# // Print the etree namespace map //
print(sh_ver_etree.nsmap)

print('\nExercise #7.5b')
# // Access the text of the "proc_board_id" element //
# //  As this XML object contains NS data, the {*} namespace wildcard  //
# // is required in the find() method. The {*} is a namespace wildcard //
# // and says to match ALL namespaces. //
print("Serial number: ", end='')
print(sh_ver_etree.find(".//{*}proc_board_id").text)