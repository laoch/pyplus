#! /usr/bin/env python3

''' Exercise #7.6 - NX-API '''

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from nxapi_plumbing import Device
import pyplus_ob

yaml_file = '/home/dobriain/PyNet_devices.yaml'
dev_str = 'nxos1'

# // Disable Self-signed Certificate Warnings //
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# // Instantiate the 'pyplusOb' class //
py_plus = pyplus_ob.pyplusOb()

# // Get device data //
_ = py_plus.get_device_data(dev_str)
dict_ = (py_plus.cisco_nxos(_[dev_str]))

# // Working on the device to clean specs //
dict_.update({'transport': 'https', 
              'verify': False, 
              'api_format': 'jsonrpc'})

for key in ['device_type', 'session_log']:
    dict_.pop(key)

device = Device(**dict_)

print('\nExercise #7.6a')
# // Send the "show interface Ethernet1/1" command to the device //
sh_intf_e1_1_all = device.show("show interface Ethernet1/1")
sh_intf_e1_1 = sh_intf_e1_1_all['TABLE_interface']['ROW_interface']

print(f"Interface: {sh_intf_e1_1['interface']}; "
      f"State: {sh_intf_e1_1['state']}; "
      f"MTU: {sh_intf_e1_1['eth_mtu']}\n")



