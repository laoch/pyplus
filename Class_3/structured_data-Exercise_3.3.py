#! /usr/bin/env python3
''' Structured Data Exercise 3.3 (JSON load) '''

import sys
import json

file = 'nxos_ssh.json'
ipv4_list = list()
ipv6_list = list()

# // De-serialise data from the file (read it) //
print(f"De-serialising data from '{file}'")
with open(file, "r") as fh:
    data = json.load(fh)

for v in data.values():
    for k2, v2 in v.items():
        if 'ipv4' in k2:
            for k3, v3 in v2.items():
                ipv4_list.append(f"{k3}/{list(v3.values())[0]}")
        else:
            for k3, v3 in v2.items():
                ipv6_list.append(f"{k3}/{list(v3.values())[0]}")

print('IPv4 List: ', ipv4_list)  
print('IPv6 List: ', ipv6_list)  

# // End //
#sys.exit(0)
