#! /usr/bin/env python3
''' Structured Data Exercise 3.4 (JSON load) '''

import sys
import json

file = 'arista.json'
out_dict = dict()

# // De-serialise data from the file (read it) //
print(f"De-serialising data from '{file}'")
with open(file, "r") as fh:
    data = json.load(fh)

# // Process the data //
for dict_ in data['ipV4Neighbors']:
    out_dict[dict_['address']] = dict_['hwAddress']

# // Output dictionary of data //
print()
print(out_dict)

# // End //
sys.exit(0)
