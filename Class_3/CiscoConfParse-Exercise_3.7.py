#! /usr/bin/env python3
''' Structured Data Exercise 3.2 (CiscoConfParse) '''

import sys
from ciscoconfparse import CiscoConfParse
ip_bgp = """
router bgp 44
 bgp router-id 10.220.88.38
 address-family ipv4 unicast
 !
 neighbor 10.220.88.20
  remote-as 42
  description pynet-rtr1
  address-family ipv4 unicast
   route-policy ALLOW in
   route-policy ALLOW out
  !
 !
 neighbor 10.220.88.32
  remote-as 43
  address-family ipv4 unicast
   route-policy ALLOW in
   route-policy ALLOW out
"""
neighbours = list()

# // Define Cisco object //
ccp_obj = CiscoConfParse(ip_bgp.splitlines())

# // Extract IP addresses and Remote AS numbers //
bgp_obj = ccp_obj.find_objects_w_parents(parentspec=r"^router",
                                         childspec=r"^\s+neighbor")

for child in bgp_obj:
    (_, ip_addr) = child.text.split()
    as_ = [x.text for x in child.children if 'remote-as' in x.text][0]
    neighbours.append((ip_addr, as_.split()[1]))

# // Print output //
print(f"\nBGP Peers:\n{neighbours}\n")

# // End //
sys.exit(0)

