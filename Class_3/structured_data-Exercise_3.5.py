#! /usr/bin/env python3
''' Structured Data Exercise 3.5 (YAML Read) '''

import sys
import pyplus
from netmiko import ConnectHandler

yaml_file = '/home/dobriain/.netmiko.yml'
log_file = 'log_file.txt'
dict_ = dict()
device = 'cisco3'

# // Get device data //
dict_ = pyplus.get_device_data(device, yaml_file)[device]

# // Add device type //
dict_.update({'session_log': 'my_output.txt', 
	          'device_type': 'cisco_ios'})

# // Connect to router //
netc = ConnectHandler(**dict_)

# // Print output //
print(f"Connecting to {device}: ")
print(netc.find_prompt())

# // End //
#sys.exit(0)
