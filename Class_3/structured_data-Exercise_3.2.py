#! /usr/bin/env python3
''' Structured Data Exercise 3.2 (YAML Write) '''

import sys
import yaml

file = 'devices.yaml'
data_list = [{'cisco3': {'host': 'cisco3.lasthop.io', 'username': 'testuser', 'password': 'testpass'}, 
              'cisco4': {'host': 'cisco4.lasthop.io', 'username': 'testuser', 'password': 'testpass'}, 
              'arista1': {'host': 'arista1.lasthop.io', 'username': 'testuser', 'password': 'testpass'}, 
              'arista2': {'host': 'arista2.lasthop.io', 'username': 'testuser', 'password': 'testpass'}, 
              'arista3': {'host': 'arista3.lasthop.io', 'username': 'testuser', 'password': 'testpass'}, 
              'arista4': {'host': 'arista4.lasthop.io', 'username': 'testuser', 'password': 'testpass'}, 
              'srx2': {'host': 'srx2.lasthop.io', 'username': 'testuser', 'password': 'testpass'}, 
              'nxos1': {'host': 'nxos1.lasthop.io', 'username': 'testuser', 'password': 'testpass'}, 
              'nxos2': {'host': 'nxos2.lasthop.io', 'username': 'testuser', 'password': 'testpass'}}]

# // Open a yaml file for writing //
with open(file, 'w+') as fh:
          fh.write('---\n')
          fh.write('\n')
          yaml.dump(data_list, fh, default_flow_style=False)

# // End //
sys.exit(0)
