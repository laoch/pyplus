#! /usr/bin/env python3
''' Structured Data Exercise 3.1 (String to complex structure) '''

sh_arp = """
Protocol  Address      Age  Hardware Addr   Type  Interface
Internet  10.220.88.1   67  0062.ec29.70fe  ARPA  Gi0/0/0
Internet  10.220.88.20  29  c89c.1dea.0eb6  ARPA  Gi0/0/0
Internet  10.220.88.22   -  a093.5141.b780  ARPA  Gi0/0/0
Internet  10.220.88.37 104  0001.00ff.0001  ARPA  Gi0/0/0
Internet  10.220.88.38 161  0002.00ff.0001  ARPA  Gi0/0/0
"""

list_ = sh_arp.split('\n')
list2_ = list()
for row in list_:
	list2_.append(row.split()) if (len(row) > 0) else next
for row in list2_:
	print(type(row), row)
