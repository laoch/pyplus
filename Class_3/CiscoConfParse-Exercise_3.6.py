#! /usr/bin/env python3
''' Structured Data Exercise 3.5 (YAML Read) '''

import sys
import pyplus
from ciscoconfparse import CiscoConfParse
from netmiko import ConnectHandler

yaml_file = '/home/dobriain/.netmiko.yml'
log_file = 'log_file.txt'
dict_ = dict()
device = 'cisco4'

# // Get device data //
dict_ = pyplus.get_device_data(device, yaml_file)[device]

# // Add device type //
dict_.update({'session_log': 'my_output.txt', 
	          'device_type': 'cisco_ios'})

# // Connect to router //
netc = ConnectHandler(**dict_)

# // Get the Cisco interface configuration //
sh_run = netc.send_command('show running-config | sec interface')


# // Define Cisco object //
ccp_obj = CiscoConfParse(sh_run.splitlines())

# // Parse the Cisco configuration //
intf_match = ccp_obj.find_objects_w_child(parentspec=r"^interface", 
                                          childspec=r"^\s+ip address")

print(f"Connecting to {device}: \n")

# // Print output //
print(f"Interface Line: {intf_match[0].text}")
ip_addr = intf_match[0].re_search_children(r'^\s+ip address')[0].text.strip()
print(f"IP Address Line: {ip_addr}")

# // End //
#sys.exit(0)
