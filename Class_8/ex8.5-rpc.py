#! /usr/bin/env python3

''' Exercise #8.4 - Configuration '''

import pyplus_ob
import sys
from pprint import pprint
from lxml import etree
from jnpr.junos import Device

yaml_file = '/home/dobriain/PyNet_devices.yaml'
dev_str = 'srx2'
common = {'timeout': 60,  'hostkey_verify': False,
          'allow_agent': False, 'look_for_keys': False}

# // Instantiate the 'pyplusOb' class //
py_plus = pyplus_ob.pyplusOb()

# ---------------------------- #
#          Functions           #
# ---------------------------- #

def main():

    # // Get device data //
    dict_ = py_plus.get_device_data(dev_str)[dev_str]

    dict_['port'] = dict_.pop('netconf_port')
    dict_['user'] = dict_.pop('username')
    del dict_['ssh_port']
    srx_dict = dict(**common, **dict_)

    # // Connect to Juniper device //
    srx_dev = Device(**srx_dict)
    srx = srx_dev.open()

    # // Get software information //
    print('\nget software information: ')
    xml_out = srx.rpc.get_software_information()
    print(etree.tostring(xml_out, encoding="unicode", 
                         pretty_print=True))
 
     # // Get interface information terse //
    print('\nget interface terse: ')
    intf_terse = srx.rpc.get_interface_information(terse=True)
    print(etree.tostring(intf_terse, encoding="unicode"))
 
     # // Get interface information terse fe-0/0/7//
    print('\nget interface fe-0/0/7 terse: ')
    intf_007 = srx.rpc.get_interface_information(terse=True, normalize=True,
                                                 interface_name='fe-0/0/7')
    print(etree.tostring(intf_007, encoding="unicode", 
                         pretty_print=True))

    return 0

if __name__ == '__main__':
    main()


# // End //
sys.exit()