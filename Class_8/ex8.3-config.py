#! /usr/bin/env python3

''' Exercise #8.3 - Configuration '''

import pyplus_ob
import sys
import jnpr.junos.exception
from jnpr.junos import Device
from jnpr.junos.utils.config import Config

yaml_file = '/home/dobriain/PyNet_devices.yaml'
dev_str = 'srx2'
common = {'timeout': 60,  'hostkey_verify': False,
          'allow_agent': False, 'look_for_keys': False}

# // Instantiate the 'pyplusOb' class //
py_plus = pyplus_ob.pyplusOb()

# ---------------------------- #
#          Functions           #
# ---------------------------- #

def main():

    # // Get device data //
    dict_ = py_plus.get_device_data(dev_str)[dev_str]

    dict_['port'] = dict_.pop('netconf_port')
    dict_['user'] = dict_.pop('username')
    del dict_['ssh_port']
    srx_dict = dict(**common, **dict_)

    # // Connect to Juniper device //
    srx_dev = Device(**srx_dict)
    srx = srx_dev.open()

    # // Create a candidate configuration object //
    cfg = Config(srx)

    # // Lock the configuration //
    cfg.lock()

    # // Try lock the configuration again //
    try:
        cfg.lock()
    except jnpr.junos.exception.LockError as e:
        print(f"  {dev_str} already connected")
        print(f"  Log: {e}")

    print('Passed the lock error')

    # // Stage configuration // 
    cfg.load("set system host-name python4life",
             format="set", merge=True)

    # // Compare candidate and running configs
    print('Before rollback\n', cfg.diff())

    # // Rollback to previous version //
    cfg.rollback(0)

    # // Compare candidate and running configs
    print('After rollback\n', cfg.diff())

    # // Unlock //
    cfg.unlock()
    
    return 0

if __name__ == '__main__':
    main()

# // End //
sys.exit()