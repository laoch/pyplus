#! /usr/bin/env python3

''' Exercise #8.2 - Tables '''

import pyplus_ob
import sys
from jnpr.junos import Device
from jnpr.junos.op.routes import RouteTable 
from jnpr.junos.op.arp import ArpTable
import pandas as pd

yaml_file = '/home/dobriain/PyNet_devices.yaml'
dev_str = 'srx2'
common = {'timeout': 60,  'hostkey_verify': False,
          'allow_agent': False, 'look_for_keys': False}

# // Instantiate the 'pyplusOb' class //
py_plus = pyplus_ob.pyplusOb()

# ---------------------------- #
#          Functions           #
# ---------------------------- #

def check_connected(device):
    ''' Check the device is connected '''
    return(device.connected)


def gatherer(device, table):
    ''' Gather tables from device '''

    dict_ = dict()

    if table == 'routes':
        data = RouteTable(device).get()
    elif table == 'arp':
        data = ArpTable(device).get()
    else:
        print(f'Error: no table {table}')
        sys.exit()

    for key in data.keys():
        dict_[key] = dict(data[key])

    return(dict_) 


def print_output(d1, d2, d3):
    ''' Print the details to the terminal '''


    def table_processing(d_):
        ''' Process the table for beautiful print '''

        # // Print table output //
        print(pd.DataFrame(d_).transpose())
        return 0

    print()
    # // Print the general details //
    for x, y in d1.items():
        print(f'{x}: {y}')

    print('\nIP routes table:')
    table_processing(d2)
    print('\nARP table:')
    table_processing(d3)
    print()

    return 0


def main():

    # // Get device data //
    dict_ = py_plus.get_device_data(dev_str)[dev_str]

    dict_['port'] = dict_.pop('netconf_port')
    dict_['user'] = dict_.pop('username')
    del dict_['ssh_port']
    srx_dict = dict(**common, **dict_)

    # // Connect to Juniper device //
    srx_dev = Device(**srx_dict)
    srx = srx_dev.open()

    # // Check the connection is successful //
    print(f'Connected to {dev_str}: ', check_connected(srx_dev))

    gen_list = {'Hostname': dict_['host'],
                'NETCONF port': dict_['port'],
                'Username': dict_['user']}
    
    # // Grap table data //
    ip_routes = gatherer(srx, 'routes')
    arp_entries = gatherer(srx, 'arp')

    print_output(gen_list, ip_routes, arp_entries)


if __name__ == '__main__':
    main()

# // End //
sys.exit()