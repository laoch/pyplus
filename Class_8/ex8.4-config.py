#! /usr/bin/env python3

''' Exercise #8.4 - Configuration '''

import pyplus_ob
import sys
import pandas as pd
from jnpr.junos import Device
from jnpr.junos.op.routes import RouteTable 
from jnpr.junos.utils.config import Config

yaml_file = '/home/dobriain/PyNet_devices.yaml'
dev_str = 'srx2'
common = {'timeout': 60,  'hostkey_verify': False,
          'allow_agent': False, 'look_for_keys': False}

# // Instantiate the 'pyplusOb' class //
py_plus = pyplus_ob.pyplusOb()

# ---------------------------- #
#          Functions           #
# ---------------------------- #

def gather_routes(device):
    ''' Gather tables from device '''

    dict_ = dict()
    data = RouteTable(device).get()

    for key in data.keys():
        dict_[key] = dict(data[key])

    return(dict_) 

def print_table(kwargs):
    ''' Print the details to the terminal '''

    print(pd.DataFrame(kwargs).transpose())

    return 0

def main():

    # // Get device data //
    dict_ = py_plus.get_device_data(dev_str)[dev_str]

    dict_['port'] = dict_.pop('netconf_port')
    dict_['user'] = dict_.pop('username')
    del dict_['ssh_port']
    srx_dict = dict(**common, **dict_)

    # // Connect to Juniper device //
    srx_dev = Device(**srx_dict)
    srx = srx_dev.open()

    # // Gather routes //
    print("\nIP Routes before change: ")
    ip_routes = gather_routes(srx)
    print_table(ip_routes)

    # // Create a candidate configuration object //
    cfg = Config(srx)

    # // Lock the configuration //
    cfg.lock()

    # // Add the new static routes //
    cfg.load(path="static_routes.conf", format="text", merge=True)

    # // Get the difference in the routing table //
    print('\n', cfg.diff())

    # // Gather routes //
    print("\nIP Routes after change: ")
    ip_routes = gather_routes(srx)
    print_table(ip_routes)    

    # // Commit the new routes //
    cfg.commit_check()
    cfg.commit(confirm=1)
    cfg.commit(comment='Static Routes added and committed')

    # // Remove the new static routes //
    cfg.load("delete routing-options static route 203.0.113.67/32", 
             format="set", merge=True)
    cfg.load("delete routing-options static route 203.0.113.68/32", 
             format="set", merge=True)
  
    # // Get the difference in the routing table //
    print('\n', cfg.diff())

    # // Commit the new routes //
    cfg.commit_check()
    cfg.commit(confirm=1)
    cfg.commit(comment='Static Routes deleted and committed')

    # // Gather routes //
    print("\nIP Routes after delete: ")
    ip_routes = gather_routes(srx)
    print_table(ip_routes)  

    # // Unlock //
    cfg.unlock()
    
    return 0

if __name__ == '__main__':
    main()


# // End //
sys.exit()