#! /usr/bin/env python3
# -*- coding: utf-8 -*-

''' Common functions for PyPlus course '''

# ============================================ #
# //             Module imports             // #
# ============================================ #

import sys
import yaml
import pyeapi
from netmiko import ConnectHandler
from pprint import pprint

# ============================================ #
# //      Global variable declarations      // #
# ============================================ #

__author__ = "Diarmuid O'Briain"
__copyright__ = "Copyright 2021, C2S"
__licence__ = "European Union Public Licence v1.2"
__version__ = "1.1"


# ############################################# #
# //              pyplusOb class             // #
# ############################################# #

class pyplusOb:
    ''' pyplusOb Class '''

    # -------------------------------------------- #
    # //           Constructor Methods          // #
    # -------------------------------------------- #

    def __init__(self):
        ''' pyplusOb class constructor method '''

        self.functionality = 'Useful Pyplus methods'
        self.yaml_file = '/home/dobriain/PyNet_devices.yaml'
        self.arista_yaml_file = '/home/dobriain/PyNet_arista_devices.yaml'
        self.log_file = 'pyplus_ob.log'

    def __str__(self):
        ''' pyplusOb class __str__ method ''' 

        return (self.functionality)

    def __repr__(self):
        ''' pyplusOb class __repr__ method ''' 

        return (f'{self.__class__.__name__}'
                f' class: {self.functionality}')    


    # -------------------------------------------- #
    # //     get_arista_device_data() Method    // #
    # -------------------------------------------- #

    def get_arista_device_data(self, device):
        ''' Get Arista data method '''

        dict_ = dict()
        dict2_ = dict()

        # // Get YAML Data //
        try:
            with open(self.arista_yaml_file) as fh: 
                dict_ = yaml.load(fh, Loader=yaml.FullLoader)
        except FileNotFoundError:
            print(f'{self.arista_yaml_file} not found') 
            sys.exit(1)

        # // Get list and dictionary of Arista devices //
        if not device in dict_.keys():
            print(f"Error: No device '{device}' in pool, skipping")
            return({})
        else:
            dict2_[device] = dict_[device]

        return(dict2_)

    # -------------------------------------------- #
    # //       get_device_data() Method         // #
    # -------------------------------------------- #

    def get_device_data(self, device):
        ''' Get data method '''

        dict_ = dict()
        dict2_ = dict()

        # // Get YAML Data //
        try:
            with open(self.yaml_file) as fh: 
                dict_ = yaml.load(fh, Loader=yaml.FullLoader)
        except FileNotFoundError:
            print(f'{self.yaml_file} not found') 
            sys.exit(1)

        # // Get list and dictionary of devices //
        if not device in dict_.keys():
            print(f"Error: No device '{device}' in pool, skipping")
            return({})
        else:
            dict2_[device] = dict_[device]

        return(dict2_)

    # -------------------------------------------- #
    # //           cisco_nxos() Method          // #
    # -------------------------------------------- #


    def cisco_nxos(self, kwargs):
        ''' Sort Cisco IOS devices method '''

        cisco_nxos_dict = {'device_type': 'cisco_nxos', 
                          'session_log': self.log_file}
        cisco_keys = ('device_type', 'host', 'username',
                      'password', 'port', 'session_log')

        # // Add cisco_ios_dict //
        kwargs = dict(kwargs, **cisco_nxos_dict)

        # // Switch ssh_port for port //
        kwargs['port'] = kwargs.pop('nxapi_port')

        # // Remove unnecessary values
        kwargs = {d: kwargs[d] for d in cisco_keys}
        
        return(kwargs)


    # -------------------------------------------- #
    # //        arista_eapi_show() Method       // #
    # -------------------------------------------- #

    def arista_eapi_show(self, cmd, kwargs):
        ''' Arista eAPI Show Method '''

        # // Connection object to Arista switch //
        con = pyeapi.client.connect(**kwargs)

        # // Create Node object //
        arista_dev = pyeapi.client.Node(con)

        # // Send show command to the device //
        out = arista_dev.enable(cmd)

        return(out)

    # -------------------------------------------- #
    # //          arista_config() Method        // #
    # -------------------------------------------- #


    def arista_eapi_config(self, cmds, kwargs):
        ''' Arista eAPI Config Method '''

        # // Connection object to Arista switch //
        con = pyeapi.client.connect(**kwargs)

        # // Create Node object //
        arista_dev = pyeapi.client.Node(con)

        # // Send configuration to the device //
        out = arista_dev.config(cmds)

        return(out)

    # -------------------------------------------- #
    # //     arista_ip_route_table() Method     // #
    # -------------------------------------------- #


    def arista_ip_route_table(self, device, kwargs):
        ''' Print IP Route table '''

        list_ = list()
        routes_dict = kwargs[0]['result']['vrfs']['default']['routes']

        # // Sort data into list for return //
        buf = 20
        list_.append(f"\nIP Route table from '{device}'")
        list_.append(f"\n{'Route':<{buf}}{'Type':<{buf}}Next-hop")
        for key, value in routes_dict.items():
            str_ = f"{key:<{buf}}{value['routeType']:<{buf}}"
            if value['routeType'] == 'static':
                str_ = str_ + f"{value['vias'][0]['nexthopAddr']:<{buf}}"
            list_.append(str_)

        return(list_)


    # -------------------------------------------- #
    # //      arista_ip_arp_table() Method      // #
    # -------------------------------------------- #


    def arista_ip_arp_table(self, device, kwargs):
        ''' Print IP ARP table '''

        list_ = list()

        # // Sort data into list for return //
        arp_tbl = kwargs[0]['result']['ipV4Neighbors']
        list_.append(f"\nMAC Address table from '{device}'")
        list_.append('\nMAC Address:\tIP Address\n')
        for arp in arp_tbl:
            list_.append(f"{arp['hwAddress']}:\t{arp['address']}")

        return(list_)

    # -------------------------------------------- #
    # //          arista_eapi() Method          // #
    # -------------------------------------------- #


    def arista_eapi(self, kwargs):
        ''' Sort Arista eAPI devices method '''

        arista_eapi_dict = {'transport': 'https'}
        arista_keys = ('transport', 'host', 'username',
                      'password', 'port')

        # // Add arista_eapi_dict //
        kwargs = dict(kwargs, **arista_eapi_dict)

        # // Switch eapi_port for port //
        kwargs['port'] = kwargs.pop('eapi_port')

        # // Remove unnecessary values
        kwargs = {d: kwargs[d] for d in arista_keys}
        
        return(kwargs)

    # -------------------------------------------- #
    # //         netmiko_show() Method          // #
    # -------------------------------------------- #


    def netmiko_connect(self, device):
        ''' Netmiko show method '''

        # // Printing the device be worked on //
        #print(f'Info: Working on device {device}')

        # // Get device data //
        dev_dict = self.get_device_data(device)

        # // Working on the device to clean specs //
        try:
            dict_ = self.cisco_nxos(dev_dict[device])
            netc = ConnectHandler(**dict_)
        except KeyError:
            print(f'No such device {device} available') 
            sys.exit(1)

        return(netc)

    # -------------------------------------------- #
    # //         netmiko_show() Method          // #
    # -------------------------------------------- #


    def netmiko_show(self, device, command, fsm=False):
        ''' Netmiko show (Single command) method '''

        out_str = str()
        out_list = list()

        # // Connect to the device and get the prompt //
        netc = self.netmiko_connect(device)

        if fsm==True:
            # // Send a single show command, return list //
            out_list = netc.send_command(command, 
                                    use_textfsm=True)
            return(out_list)
        else:
            # // Send a single show command, return string //
            out_str = netc.send_command(command, 
                                    strip_prompt=False, 
                                    strip_command=False)
            return(out_str)

    # -------------------------------------------- #
    # //         netmiko_config() Method        // #
    # -------------------------------------------- #


    def netmiko_config(self, device, commands):
        ''' Netmiko show (Multiple commands (list)) method '''

        # // Connect to the device and get the prompt //
        netc = self.netmiko_connect(device)

        # // Send a list of config command //
        netc.send_config_set(commands)

        return(netc.find_prompt())

# ######### // End pyplusOb class // ######### #

# -------------------------------------------- #
# //             main() Function            // #
# -------------------------------------------- #

def main():
    ''' main() function placeholder '''

    print('This is a module for the PyPlus course.')

    return(1)

# ============================================ #
# //                  Global                // #
# ============================================ #

# // Call main function  // 
if __name__ == "__main__":
    main()
