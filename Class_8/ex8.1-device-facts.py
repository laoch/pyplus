#! /usr/bin/env python3

''' Exercise #8.1 - Device facts '''

import pyplus_ob
import sys
from jnpr.junos import Device
from pprint import pprint

yaml_file = '/home/dobriain/PyNet_devices.yaml'
dev_str = 'srx2'

# // Common parameters //
common = {'timeout': 60,  'hostkey_verify': False,
          'allow_agent': False, 'look_for_keys': False}

# // Instantiate the 'pyplusOb' class //
py_plus = pyplus_ob.pyplusOb()

# // Get device data //
dict_ = py_plus.get_device_data(dev_str)[dev_str]

dict_['port'] = dict_.pop('netconf_port')
dict_['user'] = dict_.pop('username')
del dict_['ssh_port']
srx_dict = dict(**common, **dict_)

# // Connect to Juniper device //
srx_dev = Device(**srx_dict)
with srx_dev.open() as srx:
	facts_dict = dict(srx.facts)

# // Print output //
print(f'\n{dev_str} facts:')
pprint(facts_dict)
print('\nHostname: ', facts_dict['hostname'])

# // End //
sys.exit()
