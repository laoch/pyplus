#! /usr/bin/env python3

''' Exercise #9.1 - NAPALM Object '''

import pyplus_ob
import sys
import napalm
from pprint import pprint

yaml_file = '/home/dobriain/PyNet_devices.yaml'
dev_dict = {'cisco3':{'device_driver': 'ios'}, 
            'arista1':{'device_driver': 'eos'}}
dev_list = list()

# // Instantiate the 'pyplusOb' class //
py_plus = pyplus_ob.pyplusOb()

# ---------------------------- #
#          Functions           #
# ---------------------------- #

def main():

    # // Get device data //
    for device in dev_dict.keys():
        dict_ = py_plus.get_device_data(device)[device]
        dev_dict[device].update(**dict_)
        dev_driver = dev_dict[device]['device_driver']
        driver = napalm.get_network_driver(dev_driver)
        dh = driver(dev_dict[device]['host'], 
                    dev_dict[device]['username'], 
                    dev_dict[device]['password'])
        dev_list.append(dh)

    print()
    print("1.c. List of NAPALM connection objects to 'cisco3' and 'arista1' \n")
    for dev in dev_list:
        print(dev)
    print()

    # // Open conenction and get_facts() //

    print("1.d. NAPALM connection object get_facts() \n")
    for dev in dev_list:
        dev.open()
        pprint(dev.get_facts())
        dev.close()

    return 0

if __name__ == '__main__':
    main()


# // End //
sys.exit()