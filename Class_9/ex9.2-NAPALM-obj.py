#! /usr/bin/env python3

''' Exercise #9.2 - NAPALM Object '''

import pyplus_ob
import sys
#import napalm
from pprint import pprint

yaml_file = '/home/dobriain/PyNet_devices.yaml'
dev_dict = {'cisco3':{'device_driver': 'ios'}, 
            'arista1':{'device_driver': 'eos'}}
dev_list = list()

# // Instantiate the 'pyplusOb' class //
py_plus = pyplus_ob.pyplusOb()

# ---------------------------- #
#          Functions           #
# ---------------------------- #

def main():

    # // Get device data //
    for device in dev_dict.keys():
        dict_ = py_plus.get_device_data(device)[device]
        dict_.update(dev_dict[device])
        dev_list.append(py_plus.device_connect(dict_))

    # // Open connection and get_facts() //
    
    print()
    print("2.b. NAPALM connection object get_arp_table() \n")
    for dev in dev_list:
        dev.open()
        pprint(dev.get_arp_table())
        dev.close()
    print()

    # // Open connection and get_ntp_peers() //

    print("2.c. NAPALM connection object get_ntp_peers() \n")
    for dev in dev_list:
        dev.open()
        try:
            pprint(dev.get_ntp_peers())
        except NotImplementedError as e:
            print(f"get_ntp_peers() is not implemented for '{dev.platform}' devices")

        dev.close()
    print()

    # // Open connection and get_ntp_peers() //

    print("2.d. NAPALM Get running configurations() \n")
    for dev in dev_list:
        file = py_plus.device_get_running(dev)
        print(f'Printed the {dev.hostname} running configuration to {file}')

    print()

    return 0

if __name__ == '__main__':
    main()


# // End //
sys.exit()