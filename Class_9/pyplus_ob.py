#! /usr/bin/env python3
# -*- coding: utf-8 -*-

''' Common functions for PyPlus course '''

# ============================================ #
# //             Module imports             // #
# ============================================ #

import sys
import yaml
import napalm
from pprint import pprint

# ============================================ #
# //      Global variable declarations      // #
# ============================================ #

__author__ = "Diarmuid O'Briain"
__copyright__ = "Copyright 2021, C2S"
__licence__ = "European Union Public Licence v1.2"
__version__ = "1.1"


# ############################################# #
# //              pyplusOb class             // #
# ############################################# #

class pyplusOb:
    ''' pyplusOb Class '''

    # -------------------------------------------- #
    # //           Constructor Methods          // #
    # -------------------------------------------- #

    def __init__(self):
        ''' pyplusOb class constructor method '''

        self.functionality = 'Useful Pyplus methods'
        self.yaml_file = '/home/dobriain/PyNet_devices.yaml'
        self.arista_yaml_file = '/home/dobriain/PyNet_arista_devices.yaml'
        self.log_file = 'pyplus_ob.log'

    def __str__(self):
        ''' pyplusOb class __str__ method ''' 

        return (self.functionality)

    def __repr__(self):
        ''' pyplusOb class __repr__ method ''' 

        return (f'{self.__class__.__name__}'
                f' class: {self.functionality}')    


    # -------------------------------------------- #
    # //       get_device_data() Method         // #
    # -------------------------------------------- #

    def get_device_data(self, device):
        ''' Get data method '''

        dict_ = dict()
        dict2_ = dict()

        # // Get YAML Data //
        try:
            with open(self.yaml_file) as fh: 
                dict_ = yaml.load(fh, Loader=yaml.FullLoader)
        except FileNotFoundError:
            print(f'{self.yaml_file} not found') 
            sys.exit(1)

        # // Get list and dictionary of devices //
        if not device in dict_.keys():
            print(f"Error: No device '{device}' in pool, skipping")
            return({})
        else:
            dict2_[device] = dict_[device]

        return(dict2_)

    # -------------------------------------------- #
    # //          device_connect() Method       // #
    # -------------------------------------------- #

    def device_connect(self, kwargs):
        ''' Device connect method '''

        dev_driver = kwargs['device_driver']
        driver = napalm.get_network_driver(dev_driver)
        if dev_driver == 'nxos':
            dh = driver(kwargs['host'], 
                        kwargs['username'], 
                        kwargs['password'],
                        optional_args=kwargs['optional_args'])
            return(dh)
        else:
            dh = driver(kwargs['host'], 
                        kwargs['username'], 
                        kwargs['password'])
            return(dh)


    # -------------------------------------------- #
    # //        device_get_running() Method     // #
    # -------------------------------------------- #

    def device_get_running(self, device):
        ''' Get running configuration method '''

        device.open()
        file = f'{device.hostname}_config' 
        with open(file, mode='w') as fh:
            fh.write(device.get_config('running')['running'])
        device.close()   

        return(file)

# ######### // End pyplusOb class // ######### #

# -------------------------------------------- #
# //             main() Function            // #
# -------------------------------------------- #

def main():
    ''' main() function placeholder '''

    print('This is a module for the PyPlus course.')

    return(1)

# ============================================ #
# //                  Global                // #
# ============================================ #

# // Call main function  // 
if __name__ == "__main__":
    main()
