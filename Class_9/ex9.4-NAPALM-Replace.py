#! /usr/bin/env python3

''' Exercise #9.4 - NAPALM Replace Operations '''

import pyplus_ob
import sys
#import napalm
from pprint import pprint

yaml_file = '/home/dobriain/PyNet_devices.yaml'
nxos_dict = {'device_driver': 'nxos', 
             'optional_args': {'port': 8443}}
device = 'nxos1' 
filename = 'nxos1-checkpoint'

# // Instantiate the 'pyplusOb' class //
py_plus = pyplus_ob.pyplusOb()

# ---------------------------- #
#          Functions           #
# ---------------------------- #

def create_checkpoint(obj, filename):
    # // Retrieve a checkpoint from the NX-OS device //

    obj.open()
    checkpoint = obj._get_checkpoint_file()
    obj.close()
    with open(filename, mode='w') as fh:
        fh.write(checkpoint)

    print(f"Checkpoint file '{filename}'"
          f" created for '{obj.hostname}'")

    return 0 

def main():

    # // Device connection //
    print()
    print("4.a. Create NAPALM object for 'nxos1'\n")

    dict_ = py_plus.get_device_data(device)[device]
    dict_['port'] = dict_.pop('nxapi_port')
    del dict_['ssh_port']
    dict_ = dict(**dict_, **nxos_dict)

    # // Connect to the device //
    dev = py_plus.device_connect(dict_)
    print(dev)

    # // Get device Checkpoint //
    print()
    print("4.b. Retrieve a checkpoint from the NX-OS device\n")
    create_checkpoint(dev, filename)

    # // Replace configuration //
    print()
    print("4.d. Configuration replace operation\n")
    dev.open()
    dev.load_replace_candidate(filename='nxos1-checkpoint2')

    # // Compare and discard configuration change //
    print("After 'load_replace_candidate()'\n")
    print(dev.compare_config())
    dev.discard_config()

    # // Confirm there are no changes outstanding //
    print()
    print("After 'discard_config()'\n")
    print(dev.compare_config())

    dev.close()

    return 0

if __name__ == '__main__':
    main()


# // End //
sys.exit()