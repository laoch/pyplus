#! /usr/bin/env python3

''' Exercise #9.3 - NAPALM Merge '''

import pyplus_ob
import sys
from pprint import pprint

yaml_file = '/home/dobriain/PyNet_devices.yaml'
dev_dict = {'cisco3':{'device_driver': 'ios'}, 
            'arista1':{'device_driver': 'eos'}}
dev_list = list()

# // Instantiate the 'pyplusOb' class //
py_plus = pyplus_ob.pyplusOb()

# ---------------------------- #
#          Functions           #
# ---------------------------- #

def main():

    # // Get device data //
    for device in dev_dict.keys():
        dict_ = py_plus.get_device_data(device)[device]
        dict_.update(dev_dict[device])
        dev_list.append(py_plus.device_connect(dict_))

    # // Open connection and merge the candidate //

    print()
    print("3.b. NAPALM connection object load_merge_candidate() \n")
    for dev in dev_list:
        dev.open()
        filename = f'{dev.hostname}-loopbacks'
        print(f"\n{dev.hostname}\n{'-' * len(dev.hostname)}")
        dev.load_merge_candidate(filename=filename)
        print(dev.compare_config())
    print()

    print("3.c. NAPALM Commit the pending changes to each device \n")
    for dev in dev_list:
        filename = f'{dev.hostname}-loopbacks'
        print(f"\n{dev.hostname}\n{'-' * len(dev.hostname)}")
        dev.commit_config()
        print(dev.compare_config())
        dev.close()
    print()


    return 0

if __name__ == '__main__':
    main()


# // End //
sys.exit()